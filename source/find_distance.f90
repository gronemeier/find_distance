PROGRAM find_distance

!------------------------------------------------------------------------------!
! find closest distance to nearest obstacle within a 3D array
!---------------
! Author: Tobias Gronemeier
! Date  : 2018-02-28 (start of development)
!------------------------------------------------------------------------------!

! TODO: consider grid decomposition
! Grid decomposition has to be considered. This, however, is mostly critical to
! the input. This is of low priority and should be done directly before code is
! implemented into PALM
! 2018-03-02, gronemeier

! TODO: consider cyclic boundary conditions
! So far, only non-cyclic boundaries are considered. If boundaries are cyclic,
! topo array must contain topography of oposite boundary of a distance of the
! search radius. This is mostly an issue of how to input the topography from
! file.
! 2018-03-13, gronemeier

   USE netcdf


   implicit none


   character(len=100) :: file_in  = "./topo_input2.nc"  !< name of input file
   CHARACTER(LEN=100) :: file_out = "./distance"        !< name of output file

   INTEGER :: count_calls_short_dist = 0 !< count of calls of shortest_distance routine
   INTEGER :: count_gp_analysed = 0      !< count of grid points for which a distance is searched
   INTEGER :: count_gp_viewed = 0        !< count of grid points which were looked ad to find a distance
   INTEGER :: dist_dx       !< found distance devided by dx
   INTEGER :: i             !< loop index
   INTEGER :: ii            !< loop index
   INTEGER :: j             !< loop index
   INTEGER :: jj            !< loop index
   INTEGER :: k             !< loop index
   INTEGER :: kk            !< loop index
   INTEGER :: nx = 79       !< #grid points along x
   INTEGER :: nxl           !< grid index at left boundary
   INTEGER :: nxr           !< grid index at right boundary
   INTEGER :: ny = 79       !< #grid points along y
   INTEGER :: nyn           !< grid index at northern boundary
   INTEGER :: nys           !< grid index at southern boundary
   INTEGER :: nz = 40       !< #vertical grid levels of domain
   INTEGER :: nz_input = 15 !< #vertical grid levels of input topography
   INTEGER :: nzb           !< index of domain buttom
   INTEGER :: nzt           !< index of domain top
   INTEGER :: rad_i         !< search radius in grid points along x
   INTEGER :: rad_i_l       !< possible search radius to the left
   INTEGER :: rad_i_r       !< possible search radius to the right
   INTEGER :: rad_j         !< search radius in grid points along y
   INTEGER :: rad_j_n       !< possible search radius to north
   INTEGER :: rad_j_s       !< possible search radius to south
   INTEGER :: rad_k         !< search radius in grid points along z
   INTEGER :: rad_k_down    !< search radius in grid points along negative z
   INTEGER :: rad_k_up      !< search radius in grid points along positive z
   INTEGER :: runtime_count      !< time count used in run-time measurements
   INTEGER :: runtime_count_rate !< time-count rate used in run-time measurements

   REAL :: dx = 1.0    !< grid size along x
   REAL :: dy = 1.0    !< grid size along y
   REAL :: dz = 1.0    !< grid size along z

   REAL :: distance    !< distance between analysed grid point and a located wall
   REAL :: radius      !< search radius (metre)

   REAL, DIMENSION(0:9) :: runtime   !< run time of different code segments

   REAL, DIMENSION(:), ALLOCATABLE :: l_black !< Blackadar mixing length

   REAL, DIMENSION(:), ALLOCATABLE :: zu    !< vertical grid points, u-grid
   REAL, DIMENSION(:), ALLOCATABLE :: zw    !< vertical grid points, w-grid

   REAL, DIMENSION(:,:,:), ALLOCATABLE :: dist    !< distance/maximum mixing length at every grid point

   INTEGER(KIND=1), DIMENSION(:,:,:), ALLOCATABLE :: subarr        !< sub-array containing part of topo surrounding analysed grid point
   INTEGER(KIND=1), DIMENSION(:,:,:), ALLOCATABLE :: wall_flags_0  !< topography array


   !- Set domain indices
   nxl = 0
   nxr = nx
   nys = 0
   nyn = ny
   nzb = 0
   nzt = nz


   !- Define vertical grid (zu and zw)
   CALL get_vertical_grid


   !- Define mixing length according to Blackadar
   CALL get_mixing_length_black


   !- Read topography
   ALLOCATE(wall_flags_0(nzb:nzt+1,0:ny,0:nx))

   wall_flags_0 = 0

   CALL data_input(trim(file_in))

   !- Initialize run-time measurements
   CALL SYSTEM_CLOCK( runtime_count, runtime_count_rate )
   runtime(0) = REAL( runtime_count ) / REAL( runtime_count_rate )


   !- Calculate distance/maximum mixing length
   ALLOCATE(dist(nzb:nzt+1,0:ny,0:nx))

   dist(nzb,:,:) = 0.0

   DO  k = nzb+1, nzt  !Start at nzb+1 because l_black(nzb)=0 => dist(nzb)=0

      radius = l_black(k)

      !- Set distance to default value
      !- (maximum distance = Blackadar mixing length)
      dist(k,:,:) = radius

      !- Get search radius along x and y
      rad_i = CEILING(radius/dx)
      rad_j = CEILING(radius/dy)

      !- Get search radius in upward direction; limit it to only search up until
      !- maximum topography height is reached.
      DO kk = 0, nzt-k
         rad_k_up = kk
         IF ( zu(k+kk)-zu(k) >= radius .OR. k+kk >= nz_input )  EXIT
      ENDDO

      !- Get search radius in downward direction
      DO kk = 0, k
         rad_k_down = kk
         IF ( zu(k)-zu(k-kk) >= radius )  EXIT
      ENDDO

      rad_k = rad_k_down ! necessary for defining subarr

      !- When analysed grid point lies above maximum topography, set search
      !- radius to 0 if distance between
      !- grid point is higher than maximum search radius
      IF ( zu(k-rad_k_down) > zu(nz_input) )  rad_k_down = 0

      !- Do a search only if there is something to search for
      IF ( rad_k_down /= 0 .OR. rad_k_up /= 0 )  THEN

         ! NOTE: shape of subarr is larger in z direction
         !  Shape of subarr is two grid points larger than actual search radius
         !  in vertical direction. The first and last grid point is always set
         !  to 1.0 and is necessary so that in case there is no building along a
         !  line the MAXLOC routine does not set the first point as the maximum
         !  location
         !  2018-03-??, TG
         ALLOCATE( subarr(-rad_k-1:rad_k+1,-rad_j:rad_j,-rad_i:rad_i) )

         subarr = 1

         DO  i = 0, nx

            !- Get distance to domain border in grid points in order to
            !- know how many data can be copied into subarr
            ! TODO: indices need revision for cyclic bc
            !  These indices steer the amount of data to be copied into subarr.
            !  In case of cyclic domains, topo should have values beyond the
            !  domain boundaries, so that these indices must also be adjusted
            !  2018-03-13, TG
            rad_i_l = MIN( rad_i, i )
            rad_i_r = MIN( rad_i, nx-i )

            DO  j = 0, ny

               !- Check only for atmospheric grid points
               IF ( .NOT. BTEST( wall_flags_0(k,j,i), 0)  )  THEN

                  !- Count grid points which are analysed
                  count_gp_analysed = count_gp_analysed + 1

                  !- Reset topography within subarr
                  subarr(-rad_k:rad_k,:,:) = 0

                  !- Get distance to domain border in grid points in order to
                  !- know how many data can be copied into subarr
                  ! TODO: indices need revision for cyclic bc
                  !  These indices steer the amount of data to be copied into subarr.
                  !  In case of cyclic domains, topo should have values beyond the
                  !  domain boundaries, so that these indices must also be adjusted
                  !  2018-03-13, TG
                  rad_j_s = MIN( rad_j, j )
                  rad_j_n = MIN( rad_j, ny-j )

                  !- Copy area surrounding grid point (i/j/k) into subarr
                  !- Only copy first bit as this indicates the presence of
                  !- topography
                  DO  ii = -rad_i_l, rad_i_r
                     DO  jj = -rad_j_s, rad_j_n
                        DO  kk = -rad_k_down, rad_k_up
                           subarr(kk,jj,ii) = MERGE(1, 0,                     &
                                       BTEST(wall_flags_0(k+kk,j+jj,i+ii), 0) )
                        ENDDO
                     ENDDO
                  ENDDO

                  !- Search for walls only if walls are present within vicinity
                  IF ( MAXVAL(subarr(-rad_k:rad_k,:,:)) /= 0 )  THEN

                     !- Search within first half (positive x)
                     dist_dx = rad_i
                     DO  ii = 0, dist_dx

                        !- Search along vertical direction only if below maximum topography
                        IF ( rad_k_up > 0 ) THEN
                           !- Search for walls within octant (+++)
                           dist(k,j,i) = MIN(dist(k,j,i), &
                                             shortest_distance(subarr(0:rad_k+1,  &
                                                                      0:rad_j,    &
                                                                      ii),        &
                                                               .TRUE.,ii))

                           !- Search for walls within octant (+-+)
                           ! switch order of array so that the search always starts at the
                           ! analysed grid point (i/j/k)
                           dist(k,j,i) = MIN(dist(k,j,i), &
                                             shortest_distance(subarr(0:rad_k+1,   &
                                                                      0:-rad_j:-1, &
                                                                      ii),         &
                                                               .TRUE.,ii))

                        ENDIF

                        !- Search for walls within octant (+--)
                        dist(k,j,i) = MIN(dist(k,j,i), &
                                          shortest_distance(subarr(0:-rad_k-1:-1, &
                                                                   0:-rad_j:-1,   &
                                                                   ii),           &
                                                            .FALSE.,ii))

                        !- Search for walls within octant (++-)
                        dist(k,j,i) = MIN(dist(k,j,i), &
                                          shortest_distance(subarr(0:-rad_k-1:-1, &
                                                                   0:rad_j,       &
                                                                   ii),           &
                                                            .FALSE.,ii))

                        dist_dx = CEILING(dist(k,j,i) / dx) !reduce search along x
                                                            !by found distance
                     ENDDO

                     !- Search within second half (negative x)
                     DO  ii = 0, -dist_dx, -1

                        !- Search along vertical direction only if below maximum topography
                        IF ( rad_k_up > 0 ) THEN
                           !- Search for walls within octant (-++)
                           dist(k,j,i) = MIN(dist(k,j,i), &
                                             shortest_distance(subarr(0:rad_k+1,  &
                                                                      0:rad_j,    &
                                                                      ii),        &
                                                               .TRUE.,-ii))

                           !- Search for walls within octant (--+)
                           ! switch order of array so that the search always
                           ! starts at the analysed grid point (i/j/k)
                           dist(k,j,i) = MIN(dist(k,j,i), &
                                             shortest_distance(subarr(0:rad_k+1,   &
                                                                      0:-rad_j:-1, &
                                                                      ii),         &
                                                               .TRUE.,-ii))

                        ENDIF

                        !- Search for walls within octant (---)
                        dist(k,j,i) = MIN(dist(k,j,i), &
                                          shortest_distance(subarr(0:-rad_k-1:-1, &
                                                                   0:-rad_j:-1,   &
                                                                   ii),           &
                                                            .FALSE.,-ii))

                        !- Search for walls within octant (-+-)
                        dist(k,j,i) = MIN(dist(k,j,i), &
                                          shortest_distance(subarr(0:-rad_k-1:-1, &
                                                                   0:rad_j,       &
                                                                   ii),           &
                                                            .FALSE.,-ii))

                        dist_dx = CEILING(dist(k,j,i) / dx) !reduce search along x
                                                            !by found distance
                     ENDDO  ! ii-loop

                  ENDIF  ! check for any walls within subarr

               ELSE  ! check for building at (i,j,k)

                  dist(k,j,i) = -999.0

               ENDIF

            ENDDO  ! j-loop
         ENDDO  ! i-loop

         DEALLOCATE(subarr)

      ENDIF  ! check for vertical size of subarr

   ENDDO  ! k-loop

   dist(nzt+1,:,:) = dist(nzt,:,:)


   !- Finalize run-time measurement
   CALL SYSTEM_CLOCK( runtime_count, runtime_count_rate )
   runtime(0) = REAL( runtime_count ) / REAL( runtime_count_rate ) - runtime(0)

   WRITE( *, '(A,F7.2,A)' )  &
      '     total run time: ', runtime(0),' s'
   WRITE( *, '(A,I10)' ) &
      '     calls of "shortest_distance": ', count_calls_short_dist
   WRITE( *, '(A,I10)' ) &
      '     analysed grid points: ', count_gp_analysed
   WRITE( *, '(A,I10)' ) &
      '     viewed grid points: ', count_gp_viewed


   !- Save output
   CALL data_output("open", file_out)

   CALL data_output("write", file_out)

   CALL data_output("close", file_out)


!------------------------------------------------------------------------------!

   CONTAINS

!------------------------------------------------------------------------------!

   REAL FUNCTION shortest_distance( array, orientation, pos_i )
   ! Return the shortest distance between grid point array(0,0) and a wall
   ! within that array.

      LOGICAL, INTENT (IN) :: orientation !< flag if searched upwards (true) or downwards (false)

      INTEGER, INTENT (IN) :: pos_i     !< position at which it is searched, origin=point for which distance is calculated

      INTEGER :: jj        !< loop index

      INTEGER, DIMENSION(0:rad_j) :: loc_k !< location of closest wall along z

      INTEGER(KIND=1), DIMENSION(0:rad_k+1,0:rad_j) :: array !< array to be searched within

      !- Get coordinate of first maximum along vertical dimension
      !- at each y position of array.
      !- Substract 1 because indices count from 1 instead of 0 by MAXLOC
      loc_k = MAXLOC( array, DIM = 1) - 1

      !- Set distance to the default maximum value (=search radius)
      shortest_distance = radius

      !- Calculate distance between position (0/0/0) and
      !- position (pos_i/jj/loc(jj)) and only save the shortest distance.
      IF ( orientation ) THEN !if array is oriented upwards
         DO  jj = 0, rad_j
            shortest_distance = MIN(shortest_distance,                        &
                                    SQRT( MAX(pos_i*dx-0.5*dx,0.0)**2         &
                                        + MAX(jj*dy-0.5*dy,0.0)**2            &
                                        + MAX(zw(loc_k(jj)+k-1)-zu(k),0.0)**2 &
                                        )                                     &
                                   )
         ENDDO
      ELSE  !if array is oriented downwards
         ! NOTE: MAX within zw required to circumvent error at domain border
         !  At the domain border, if non-cyclic boundary is present, the
         !  index for zw could be -1, which will be errorneous (zw(-1) does
         !  not exist). The MAX function limits the index to be at least 0.
         DO  jj = 0, rad_j
            shortest_distance = MIN(shortest_distance,                      &
                                    SQRT( MAX(pos_i*dx-0.5*dx,0.0)**2       &
                                        + MAX(jj*dy-0.5*dy,0.0)**2          &
                                        + MAX(zu(k)-zw(MAX(k-loc_k(jj),     &
                                                           0)),             &
                                              0.0)**2                       &
                                        )                                   &
                                   )
         ENDDO
      ENDIF

      !- count grid points which were looked at to calculate a distance
      count_gp_viewed = count_gp_viewed + rad_j + 1

      !- count calls of function
      count_calls_short_dist = count_calls_short_dist + 1

   END FUNCTION

!------------------------------------------------------------------------------!

   SUBROUTINE get_vertical_grid
   ! Define zu and zw (exactly same definition as in PALM)

      REAL :: dz_max = 5.0             !< maximum vertical grid spacing
      REAL :: dz_stretch_factor = 1.04 !< stretching factor of vertical grid stretching
      REAL :: dz_stretch_level = 100.0 !< height at which vertical grid stretching starts
      REAL :: dz_stretched             !< stretched vertical grid spacing

      ALLOCATE(zu(nzb:nzt+1))
      ALLOCATE(zw(nzb:nzt+1))

      zu(0) = 0.0
      zu(1) = dz * 0.5

      dz_stretched = dz
      DO  k = 2, nzt+1
         IF ( dz_stretch_level <= zu(k-1)  .AND.  dz_stretched < dz_max )  THEN
            dz_stretched = dz_stretched * dz_stretch_factor
            dz_stretched = MIN( dz_stretched, dz_max )
         ENDIF
         zu(k) = zu(k-1) + dz_stretched
      ENDDO

      zw(0) = 0.0
      DO  k = 1, nzt
         zw(k) = ( zu(k) + zu(k+1) ) * 0.5
      ENDDO
      zw(nzt+1) = zw(nzt) + 2.0 * ( zu(nzt+1) - zw(nzt) )

   END SUBROUTINE get_vertical_grid

!------------------------------------------------------------------------------!

   SUBROUTINE get_mixing_length_black
   ! Calculate mixing length according to Blackadar

      REAL :: f = 1.0E-4         !< Coriolis parameter
      REAL :: kappa = 0.4        !< von-Karman constant
      REAL :: l_max              !< maximum mixing length
      REAL :: wind_speed = 10.0  !< geostrophic wind speed at domain top

      ALLOCATE(l_black(nzb:nzt+1))

      ! QUESTION: Set l_max to 100m?
      !  l_max is calculated like in PALM (according to Blackadar, 1962); however,
      !  according to Lopez (2002) it should be constant and according to Salim
      !  (2018) it should be l_max = 100m.
      !  2018-03-12, TG
      IF ( f /= 0.0 )  THEN
         l_max = 2.7E-4 * wind_speed / ABS( f ) + 1E-10
      ELSE
         l_max = 30.0
      ENDIF

      DO  k = nzb, nzt
         l_black(k) = kappa * zu(k) / ( 1.0 + kappa * zu(k) / l_max )
      ENDDO

      l_black(nzt+1) = l_black(nzt)

   END SUBROUTINE get_mixing_length_black

!------------------------------------------------------------------------------!

   SUBROUTINE data_input(input)

      CHARACTER(LEN=*), INTENT(IN) :: input  !< name of input file

      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_in       !< ID of topography
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines
      INTEGER, DIMENSION(1:3)  ::  id_dim
      INTEGER :: n_file

      INTEGER(KIND=1), DIMENSION(0:nx,0:ny,0:nz_input) :: var_in

      var_in = 0

      nc_stat = NF90_OPEN( input, NF90_NOWRITE, id_file )

      nc_stat = NF90_INQ_VARID( id_file, "buildings_3D", id_var_in )

      nc_stat = NF90_GET_VAR( id_file, id_var_in, var_in,        &
                            start = (/ 1, 1, 1 /),               &
                            count = (/ nx+1, ny+1, nz_input+1 /) )

      DO  i = 0, nx
         DO  j = 0, ny
            DO  k = nzb, nz_input-2
               IF ( var_in(i,j,k) == 1 )  THEN
                  wall_flags_0(k,j,i) = IBSET( wall_flags_0(k,j,i), 0 )
                  wall_flags_0(k,j,i) = IBSET( wall_flags_0(k,j,i), 1 )
               ENDIF
            ENDDO
            DO  k = nz_input-1, nz_input
               IF ( var_in(i,j,k) == 1 )  &
                  wall_flags_0(k,j,i) = IBSET( wall_flags_0(k,j,i), 0 )
            ENDDO
         ENDDO
      ENDDO

   END SUBROUTINE data_input

!------------------------------------------------------------------------------!

   SUBROUTINE data_output(action, output)
   ! Data output to NetCDF file

      CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine (open/close/write)
      CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix

      INTEGER, SAVE :: id_dim_x        !< ID of dimension x
      INTEGER, SAVE :: id_dim_y        !< ID of dimension y
      INTEGER, SAVE :: id_dim_zu       !< ID of dimension zu
      INTEGER, SAVE :: id_dim_zw       !< ID of dimension zw
      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_dist     !< ID of distance
      INTEGER, SAVE :: id_var_lb       !< ID of mixing length (Blackadar)
      INTEGER, SAVE :: id_var_topo     !< ID of topography
      INTEGER, SAVE :: id_var_x        !< ID of x
      INTEGER, SAVE :: id_var_y        !< ID of y
      INTEGER, SAVE :: id_var_zu       !< ID of zu
      INTEGER, SAVE :: id_var_zw       !< ID of zw
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines

      REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array

      REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array

      REAL, DIMENSION(:,:,:), ALLOCATABLE :: netcdf_data_3d  !< 3D output array

      SELECT CASE (TRIM(action))

         !- Initialize output file
         CASE ('open')

            !- Delete any pre-existing output file
            OPEN(20, FILE=TRIM(output)//'.nc')
            CLOSE(20, STATUS='DELETE')

            !- Open file
            nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
            IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'

            !- Define spatial coordinates
            nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
            nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
                                    id_dim_x, id_var_x)
            nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
            nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
                                    id_dim_y, id_var_y)
            nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'zu', nzt+2-nzb, id_dim_zu)
            nc_stat = NF90_DEF_VAR(id_file, 'zu', NF90_DOUBLE,  &
                                    id_dim_zu, id_var_zu)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zu, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'zw', nzt+2-nzb, id_dim_zw)
            nc_stat = NF90_DEF_VAR(id_file, 'zw', NF90_DOUBLE,  &
                                    id_dim_zw, id_var_zw)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zw, 'units', 'meters')

            !- Define output arrays
            nc_stat = NF90_DEF_VAR(id_file, 'l_black', NF90_DOUBLE,  &
                                    id_dim_zu, id_var_lb)
            nc_stat = NF90_PUT_ATT(id_file, id_var_lb, 'units', 'meters')

            nc_stat = NF90_DEF_VAR(id_file, 'distance', NF90_DOUBLE,   &
                                    (/id_dim_x, id_dim_y, id_dim_zu/),  &
                                    id_var_dist)
            nc_stat = NF90_PUT_ATT(id_file, id_var_dist,  &
                                    'long_name', 'distance')
            nc_stat = NF90_PUT_ATT(id_file, id_var_dist, 'units', 'm')
            nc_stat = NF90_PUT_ATT(id_file, id_var_dist, '_FillValue', &
                                  REAL( -999.0, KIND = 8 ) )

            nc_stat = NF90_DEF_VAR(id_file, 'topo', NF90_DOUBLE,       &
                                    (/id_dim_x, id_dim_y, id_dim_zu/),  &
                                    id_var_topo)
            nc_stat = NF90_PUT_ATT(id_file, id_var_topo,  &
                                    'long_name',            &
                                    'topography')
            nc_stat = NF90_PUT_ATT(id_file, id_var_topo, 'units', '1')

            !- Leave define mode
            nc_stat = NF90_ENDDEF(id_file)

            !- Write axis to file

            !- x axis
            ALLOCATE(netcdf_data_1d(0:nx))
            DO  i = 0, nx
               netcdf_data_1d(i) = i * dx + 0.5 * dx
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
                                    start = (/1/), count = (/nx+1/))
            DEALLOCATE(netcdf_data_1d)

            !- y axis
            ALLOCATE(netcdf_data_1d(0:ny))
            DO  j = 0, ny
               netcdf_data_1d(j) = j * dy + 0.5 * dx
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
                                    start = (/1/), count = (/ny+1/))
            DEALLOCATE(netcdf_data_1d)

            !- zu axis
            ALLOCATE(netcdf_data_1d(nzb:nzt+1))
            netcdf_data_1d = zu
            nc_stat = NF90_PUT_VAR(id_file, id_var_zu, netcdf_data_1d,  &
                                    start = (/1/), count = (/nzt+2-nzb/))

            !- zw axis
            netcdf_data_1d = zw
            nc_stat = NF90_PUT_VAR(id_file, id_var_zw, netcdf_data_1d,  &
                                    start = (/1/), count = (/nzt+2-nzb/))

            !- l_black
            netcdf_data_1d = l_black
            nc_stat = NF90_PUT_VAR(id_file, id_var_lb, netcdf_data_1d,  &
                                    start = (/1/), count = (/nzt+2-nzb/))
            DEALLOCATE(netcdf_data_1d)


         !- Close NetCDF file
         CASE ('close')

            nc_stat = NF90_CLOSE(id_file)


         !- Write data arrays to file
         CASE ('write')

            ALLOCATE(netcdf_data_3d(0:nx,0:ny,nzb:nzt+1))

            !- Write distance
            DO  i = 0, nx
               DO  j = 0, ny
                  DO  k = nzb, nzt+1
                     netcdf_data_3d(i,j,k) = dist(k,j,i)
                  ENDDO
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_dist, netcdf_data_3d, &
                                    start = (/1, 1, 1/),                 &
                                    count = (/nx+1, ny+1, nzt+2-nzb/))

            !- Write topography
            DO  i = 0, nx
               DO  j = 0, ny
                  DO  k = nzb, nzt+1
                     netcdf_data_3d(i,j,k) = wall_flags_0(k,j,i)
                  ENDDO
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_topo, netcdf_data_3d, &
                                    start = (/1, 1, 1/),                 &
                                    count = (/nx+1, ny+1, nzt+2-nzb/))

            DEALLOCATE(netcdf_data_3d)


         !- Print error message if unknown action selected
         CASE DEFAULT

            WRITE(*, '(A)') ' ** data_output: action "'//  &
                              TRIM(action)//'"unknown!'

      END SELECT

   END SUBROUTINE data_output

END PROGRAM find_distance
