find_distance
=============

A program to find the closest distance to the next obstacle for every grid point on a Cartesian grid.